# Ganache CapRover Deployment

Ganache Deployment to CapRover Hosting

<img src="https://gitlab.com/pascal.cantaluppi/ganache/-/raw/main/public/ganache.png?ref_type=heads" />

## Configuration file

Setup for JavaScipt Clients:

```JavaScript
  networks: {
    ganache: {
      host: 'your_fqdn_here',
      port: 8545,
      network_id: '5777',
    },
  },
```

## CapRover Hosting

For more information about free and open source paas visit:

https://caprover.com/
