FROM node:alpine
WORKDIR /app
RUN npm i -g ganache-cli --silent
EXPOSE 8545
CMD ["ganache-cli", "-h", "0.0.0.0", "--port", "8545", "--networkId", "5777"] 